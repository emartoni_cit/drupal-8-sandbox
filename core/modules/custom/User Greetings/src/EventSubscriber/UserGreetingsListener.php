<?php
/**
 * Created by PhpStorm.
 * User: emartoni
 * Date: 05/02/18
 * Time: 15:58
 */

namespace Drupal\user_greetings\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UserGreetingsListener implements EventSubscriberInterface {

  private $loggerFactory;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {

    $this->loggerFactory = $loggerChannelFactory;

  }

  public function GetResponseEvent(GetResponseEvent $event) {

    $request = $event->getRequest();
    $shouldLog = strpos($request->getPathInfo(), '/welcome');

    if ($shouldLog !== FALSE) {
        $this->loggerFactory->get('default')
          ->debug('Listener Get!');
    }

  }


  public static function getSubscribedEvents() {

    return [
      KernelEvents::REQUEST => 'GetResponseEvent',
    ];

  }
}