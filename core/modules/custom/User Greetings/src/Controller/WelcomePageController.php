<?php

namespace Drupal\user_greetings\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user_greetings\Service\WelcomeMessageGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class WelcomePageController extends ControllerBase {

  private $welcomeMessageGenerator;

  public function __construct(WelcomeMessageGenerator $welcomeMessageGenerator) {

    $this->welcomeMessageGenerator = $welcomeMessageGenerator;

  }

  public function welcomeMsg($username) {

    $welcome_msg = $this->welcomeMessageGenerator->getWelcomeMessage($username);

    return new Response($welcome_msg);
  }

  public static function create(ContainerInterface $container) {

    $welcomeGenerator = $container->get('user_greetings.welcome_message_generator');
    return new static($welcomeGenerator);

  }


}