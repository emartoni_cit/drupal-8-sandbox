<?php
/**
 * Created by PhpStorm.
 * User: emartoni
 * Date: 05/02/18
 * Time: 15:02
 */

namespace Drupal\user_greetings\Service;


class WelcomeMessageGenerator {

  public function getWelcomeMessage($username) {

    if (is_numeric($username)) {
      return 'Welcome to my new Drupal 8 website!';
    }

    return sprintf('Welcome %s to my new Drupal 8 website!', $username);

  }

}